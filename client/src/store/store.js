import { createStore } from 'redux';

import initialHorsesState from '../constants';

function horseReducer(state = initialHorsesState, action) {
  if (action.type === 'start game') {
    return action.payload;
  }

  return state;
}

const store = createStore(horseReducer);

export default store;
