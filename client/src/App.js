import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import io from 'socket.io-client';

import Header from './components/Header';
import Field from './components/Field';
import Button from './components/Button';
import Info from './components/Info';

import galopSound from './assets/galopSound.wav';
import horseSound from './assets/horseSound.wav';
import shot from './assets/shot.mp3';

import './App.css';

const socket = io('ws://localhost:3002/');

function App() {
  const [disabled, setDisabled] = useState(false);
  const dispatch = useDispatch();

  const sound = new Audio(galopSound);
  const sound2 = new Audio(horseSound);
  const sound3 = new Audio(shot);

  useEffect(() => {
    socket.on('connect', () => {
      socket.emit('start');
    });
    socket.on('disconnect', () => {
      socket.emit('disconnect');
    });
  }, []);

  function startGame() {
    setDisabled(true);

    sound.loop = true;
    sound.play();
    sound3.play();
    setTimeout(() => {
      sound2.play();
    }, 700);

    socket.on('ticker', (data) => {
      dispatch({ type: 'start game', payload: data });

      const winner = data.find((el) => el.distance === 1000);
      if (winner) {
        socket.off('connect');
        socket.off('disconnect');
        socket.off('ticker');
        sound.pause();
        alert(`${winner.name} is a winner`);
      }
    });
  }

  return (
    <div className="wrapper">
      <Header />
      <Field />
      <Info />
      <Button onClick={startGame} disabled={disabled}>
        Start
      </Button>
    </div>
  );
}

export default App;
