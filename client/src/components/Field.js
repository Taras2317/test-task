import React from 'react';
import { useSelector } from 'react-redux';

import field from '../assets/field.jpg';
import horse1 from '../assets/horse1.png';
import horse2 from '../assets/horse2.png';
import horse3 from '../assets/horse3.png';
import horse4 from '../assets/horse4.png';
import horse5 from '../assets/horse5.png';
import horse6 from '../assets/horse6.png';

import classes from './Field.module.css';

function Field() {
  const horses = useSelector((state) => state);

  return (
    <div className={classes.field}>
      <img src={field} alt="field" className={classes.track}></img>
      <img
        style={{ left: horses[0].distance - 175 }}
        src={horse1}
        alt="horse1"
        className={`${classes.horse} ${classes.horse1}`}
      ></img>
      <img
        style={{ left: horses[1].distance - 175 }}
        src={horse2}
        alt="horse2"
        className={`${classes.horse} ${classes.horse2}`}
      ></img>
      <img
        style={{ left: horses[2].distance - 175 }}
        src={horse3}
        alt="horse3"
        className={`${classes.horse} ${classes.horse3}`}
      ></img>
      <img
        style={{ left: horses[3].distance - 175 }}
        src={horse4}
        alt="horse4"
        className={`${classes.horse} ${classes.horse4}`}
      ></img>
      <img
        style={{ left: horses[4].distance - 175 }}
        src={horse5}
        alt="horse5"
        className={`${classes.horse} ${classes.horse5}`}
      ></img>
      <img
        style={{ left: horses[5].distance - 175 }}
        src={horse6}
        alt="horse6"
        className={`${classes.horse} ${classes.horse6}`}
      ></img>
    </div>
  );
}

export default Field;
