import React from 'react';

import logo from '../assets/logo.png';
import classes from './Header.module.css';

function Header() {
  return (
    <header className={classes.header}>
      <h1>Horse racing Game</h1>
      <img src={logo} alt="logo"></img>
    </header>
  );
}

export default Header;
