import React from 'react';

import classes from './Info.module.css';

function Info() {
  return (
    <div className={classes.info}>
      <span className={classes.horse1}>Princess Diana</span>
      <span className={classes.horse2}>Cricket</span>
      <span className={classes.horse3}>Rebel</span>
      <span className={classes.horse4}>Lucy</span>
      <span className={classes.horse5}>Lacey</span>
      <span className={classes.horse6}>Ginger</span>
    </div>
  );
}

export default Info;
